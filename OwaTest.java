
import org.openqa.selenium.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.*;
import java.util.stream.*;
import java.util.stream.Collectors.*;

public class OwaTest {


    public static void main(String[] args) {
        System.out.println("Web Testing");
        test();
    }


//    @Test
    public static void test() {
      System.setProperty("webdriver.chrome.driver", "/usr/lib/chromium-browser/chromedriver");

      WebDriver driver = new ChromeDriver();
      driver.get("https://owa.apptixemail.net/owa/");
      try {
          Thread.sleep(1000);  // Let the user actually see something!


//id username -> email
//id password -> pw
          By nameElem = By.name("username");
          By passElem = By.name("password");
          By subElem = By.tagName("button");
          By formElem = By.tagName("form");
          WebElement name = driver.findElement(nameElem);
          WebElement pass = driver.findElement(passElem);
//          WebElement sub = driver.findElement(subElem);
          WebElement form = driver.findElement(formElem);
          name.sendKeys("<<USERNAME_HERE_NOT_SO_GOOD>>");
          Thread.sleep(2000);
          pass.sendKeys("<<PASSWORD_HERE_BAD>>\n");
          Thread.sleep(2000);
//          form.submit();


          By aTag = By.tagName("a");
          List<WebElement> links = driver.findElements(aTag);
          System.out.println("Link count: "+links.size());
          Thread.sleep(5000);  // what happened... link followed :D
          String linkDisplay = links.stream().map( -> ).collect(Collectors.joining("..."));

//          driver.quit(); //just hang
      } catch (Exception e) {
          System.out.println("\n\nError:\n"+e);
      }
    }



}
