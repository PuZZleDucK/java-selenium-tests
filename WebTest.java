
import org.openqa.selenium.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.*;

public class WebTest {


    public static void main(String[] args) {
        System.out.println("Web Testing");
        testGoogleSearch();
        
        //selenium
        //setUp("http://localhost:8080/", "*chrome");
        //selenium.open("/BrewBizWeb/");
        
    }


//    @Test
    public static void testGoogleSearch() {
      System.setProperty("webdriver.chrome.driver", "/usr/lib/chromium-browser/chromedriver");

      WebDriver driver = new ChromeDriver();
      driver.get("http://www.google.com/");
      try {
          Thread.sleep(1000);  // Let the user actually see something!
          WebElement searchBox = driver.findElement(By.name("q"));
          
          searchBox.sendKeys("Ch");
          Thread.sleep(2000);
          searchBox.sendKeys("rome");
          Thread.sleep(1000);
          searchBox.sendKeys("Driver");
          Thread.sleep(1000);
          
          searchBox.submit();

          By nameR = By.className("r");
          System.out.println("Finding elements:");
          WebElement result = driver.findElement(nameR);
          System.out.println("REz: "+result.toString());
          By aTag = By.tagName("a");
          WebElement resultLink = result.findElement(aTag);
          System.out.println("REF: "+resultLink.getAttribute("href"));

          Thread.sleep(5000);  // Let the user actually see something!
          driver.get(resultLink.getAttribute("href"));

          List<WebElement> links = driver.findElements(aTag);
          System.out.println("Link count: "+links.size());
          Thread.sleep(5000);  // what happened... link followed :D

        driver.quit();
      } catch (Exception e) {
          System.out.println("\n\nError:\n"+e);
      }
    }



}
